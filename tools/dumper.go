package main

import (
	"flag"
	"fmt"
	"log/slog"
	"slices"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

var msgCnt int = 0

func dumpMessage(msg *xaal.Message) {
	msgCnt++
	fmt.Println("Message:", msgCnt)
	msg.Dump()
	fmt.Println()
}

func main() {
	xaal.SetupLogger(slog.LevelInfo)

	// parse cmd line
	flagAliveFilter := flag.Bool("alive", true, "Enable/Disable Alive filtering")
	flagUUID := flag.String("target", "", "UUID to filter messages")
	flag.Parse()

	// check cmd line args
	var target *uuid.UUID
	if len(*flagUUID) > 0 {
		tmp, err := uuid.FromString(*flagUUID)
		if err != nil {
			slog.Error("Invalid UUID", "err", err)
			return
		}
		target = &tmp
		slog.Info("Filtering on UUID:", "uuid", target)
	}

	showMSG := func(msg *xaal.Message) {
		if (*flagAliveFilter) && msg.IsAlive() {
			return
		}
		// we have a target, we the source or the target, if no return
		if target != nil && (msg.Source != *target && !slices.Contains(msg.Targets, *target)) {
			return
		}

		dumpMessage(msg)
	}

	eng := xaal.NewEngine()
	eng.DisableMessageFilter()

	eng.Subscribe(showMSG)
	eng.Run()
}
