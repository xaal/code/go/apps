//go:build ignore

package main

import (
	"log/slog"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

func main() {
	xaal.SetupLogger(slog.LevelDebug)
	eng := xaal.NewEngine()

	lamp := xaal.NewDevice("lamp.dimmer")
	lamp.Address, _ = uuid.FromString("6558b72b-3ae6-4995-8c4c-e407b7119889")

	light := lamp.AddAttribute("light", true)
	brightness := lamp.AddAttribute("brightness", 50)

	lamp.VendorID = "IMT Atlantique"
	lamp.ProductID = "Golang test lamp"
	lamp.URL = "http://example.com"
	lamp.HWID = "0x1234"
	lamp.Info = "Random info"

	turn_on := func(xaal.MessageBody) *xaal.MessageBody {
		light.SetValue(true)
		return nil
	}

	turn_off := func(xaal.MessageBody) *xaal.MessageBody {
		light.SetValue(false)
		return nil
	}

	toggle := func(xaal.MessageBody) *xaal.MessageBody {
		light.SetValue(!light.Value.(bool))
		return nil
	}

	dim := func(args xaal.MessageBody) *xaal.MessageBody {
		if value, ok := args["brightness"].(uint64); ok {
			if value > 100 {
				value = 100
			}
			brightness.SetValue(int(value))
			if value == 0 {
				turn_off(nil)
			} else {
				turn_on(nil)
			}
		}
		return nil
	}

	lamp.AddMethod("turn_on", turn_on)
	lamp.AddMethod("turn_off", turn_off)
	lamp.AddMethod("toggle", toggle)
	lamp.AddMethod("set_brightness", dim)

	lamp.Dump()
	eng.AddDevice(lamp)

	eng.Run()
}
