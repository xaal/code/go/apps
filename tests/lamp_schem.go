//go:build ignore

package main

import (
	"log/slog"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/schemas"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

func main() {
	xaal.SetupLogger(slog.LevelDebug)
	eng := xaal.NewEngine()
	eng.Start()

	addr, _ := uuid.FromString("820f3c5c-029a-4799-8105-8d6d5357462b")
	grpID, _ := addr.Add(0xeeff)
	lamp := schemas.NewLampDimmer(addr)
	lamp.Info = "Fake (Go)lamp from schemas"
	lamp.HWID = "0x1234"
	lamp.VendorID = "IMT Atlantique"
	lamp.ProductID = "Golang test lamp"
	lamp.GroupID = grpID
	lamp.Dump()

	light := lamp.GetAttribute("light")
	brightness := lamp.GetAttribute("brightness")
	// change the brightness type to int ;)
	brightness.Value = 50

	light.SetValue(true)
	// Switch on the lamp
	turnOn := func(xaal.MessageBody) *xaal.MessageBody {
		light.SetValue(true)
		brightness.SetValue(brightness.Value.(int) + 10)
		return nil
	}
	// Switch off the lamp
	turnOff := func(xaal.MessageBody) *xaal.MessageBody {
		light.SetValue(false)
		brightness.SetValue(brightness.Value.(int) - 10)
		return nil
	}
	lamp.SetMethod("turn_on", turnOn)
	lamp.SetMethod("turn_off", turnOff)

	// We add a button for GroupID test purpose
	addr, _ = addr.Add(1)
	btn := schemas.NewButton(addr)
	btn.Info = "Fake button from schemas"
	btn.GroupID = grpID
	btn.Dump()

	eng.AddDevice(lamp)
	eng.AddDevice(btn)
	eng.Run()
}
