//go:build ignore

// This application is a very simple example on how to use the xAAL library.
// It is a chandelier with 6 dimming lamps.

package main

import (
	"log/slog"
	"time"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

type Lamp struct {
	dev *xaal.Device
}

func newLamp(addr uuid.UUID, group uuid.UUID) *Lamp {
	dev := xaal.NewDevice("lamp.dimmer")
	lamp := &Lamp{dev: dev}
	dev.Address = addr
	dev.VendorID = "IMT Atlantique"
	dev.ProductID = "Golang Chandelier Lamp"
	dev.GroupID = group
	dev.AddAttribute("light", false)
	dev.AddAttribute("brightness", 50)
	dev.AddMethod("turn_on", lamp.turnOn)
	dev.AddMethod("turn_off", lamp.turnOff)
	dev.AddMethod("toggle", lamp.toggle)
	dev.AddMethod("set_brightness", lamp.setBrightness)
	dev.Dump()
	return lamp
}

// turnOn set the brightness to 50 to test attributes grouped msg
func (lamp *Lamp) turnOn(xaal.MessageBody) *xaal.MessageBody {
	light := lamp.dev.GetAttribute("light")
	brightness := lamp.dev.GetAttribute("brightness")

	if light.Value == true {
		return nil
	}
	light.SetValue(true)
	if brightness.Value == 0 {
		brightness.SetValue(50)
	}
	return nil
}

// turnOff set the brightness to 0 to test attributes grouped msg
func (lamp *Lamp) turnOff(xaal.MessageBody) *xaal.MessageBody {
	light := lamp.dev.GetAttribute("light")
	brightness := lamp.dev.GetAttribute("brightness")

	if light.Value == false {
		return nil
	}
	light.SetValue(false)
	brightness.SetValue(0)
	return nil
}

// toggle the light, to blink it
func (lamp *Lamp) toggle(xaal.MessageBody) *xaal.MessageBody {
	light := lamp.dev.GetAttribute("light")
	if light.Value == true {
		lamp.turnOff(nil)
	} else {
		lamp.turnOn(nil)
	}
	return nil
}

func (lamp *Lamp) setBrightness(args xaal.MessageBody) *xaal.MessageBody {
	if value, ok := args["brightness"].(uint64); ok {
		if value > 100 {
			value = 100
		}
		lamp.dev.GetAttribute("brightness").SetValue(int(value))
	}
	return nil
}

// used to make it blink w/ a goroutine
func (lamp *Lamp) blink(xaal.MessageBody) *xaal.MessageBody {
	for {
		lamp.toggle(nil)
		time.Sleep(time.Second * 3)
	}
}

func main() {
	xaal.SetupLogger(slog.LevelDebug)
	eng := xaal.NewEngine()
	eng.Start()

	baseAddr, _ := uuid.FromString("89b11138-a76b-11ee-91c7-d6bd5fe10000")
	group, _ := baseAddr.Add(0xeeff)
	for i := int64(0); i < 6; i++ {
		addr, _ := baseAddr.Add(i)
		l := newLamp(addr, group)
		eng.AddDevice(l.dev)
		if i == 0 {
			go l.blink(nil)
		}
	}

	eng.Run()
}
