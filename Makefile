all:tidy dumper chandelier lamp lamp_schem

tidy:
	go mod tidy

dumper:
	go build -o bin/dumper tools/dumper.go
	strip bin/dumper

chandelier:
	go build -o bin/chandelier tests/chandelier.go
	strip bin/chandelier

lamp:
	go build -o bin/lamp tests/lamp.go
	strip bin/lamp

lamp_schem:
	go build -o bin/lamp_schem tests/lamp_schem.go
	strip bin/lamp_schem

clean:
	rm -f bin/*
